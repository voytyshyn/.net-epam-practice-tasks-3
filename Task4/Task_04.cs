using System;

namespace Task_04
{
    public sealed class Matrix: IEquatable<Matrix>
    {
        #region Fields
        private double[,] _matrix;
        #endregion

        #region Constructors
        public Matrix(int rows = 10, int cols = 10)
        {
            if (rows <= 0 || cols <= 0)
                throw new ArgumentOutOfRangeException();

            this.Rows = rows;
            this.Cols = cols;
            _matrix = new double[Rows, Cols];
        }
        #endregion

        #region Properties
        public int Rows { get; private set; }
        public int Cols { get; private set; }
        #endregion
        
        #region Indexer
        public double this[int rows, int cols]
        {
            get
            {
                if ((rows < 0 || rows >= this.Rows) || (cols < 0 || cols >= this.Cols))
                   throw new ArgumentOutOfRangeException();
                return _matrix[rows, cols];
            }

            set
            {
                if ((rows < 0 || rows >= this.Rows) || (cols < 0 || cols >= this.Cols))
                   throw new ArgumentOutOfRangeException();
                _matrix[rows, cols] = value;
            }
        }
        #endregion

        #region Methods
        // Addition of 2 matrix.
        public static Matrix Add(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null || matrix2 == null)
                throw new ArgumentNullException();
            
            if (matrix1.Rows != matrix2.Rows || matrix1.Cols != matrix2.Cols)
            {
                throw new ArgumentException("Wrong dimensions of matrix! They must be equal.");
            }

            Matrix temp = new Matrix(matrix1.Rows, matrix1.Cols);
            for (var i = 0; i < temp.Rows; i++)
                for (var j = 0; j < temp.Cols; j++)
                    temp[i, j] = matrix1[i, j] + matrix2[i, j];

            return temp;
        }
        
        public Matrix Add(Matrix matrix)
        {
            if (matrix == null)
                throw new ArgumentNullException();
            
            if (this.Rows != matrix.Rows || this.Cols != matrix.Cols)
            {
                throw new ArgumentException("Wrong dimensions of matrix! They must be equal.");
            }

            return Matrix.Add(this, matrix);
        }
        
        // Substraction of 2 matrix.
        public static Matrix Substract(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null || matrix2 == null)
                throw new ArgumentNullException();
            
            if (matrix1.Rows != matrix2.Rows || matrix1.Cols != matrix2.Cols)
            {
                throw new ArgumentException("Wrong dimensions of matrix! They must be equal.");
            }

            Matrix temp = new Matrix(matrix1.Rows, matrix1.Cols);
            for (var i = 0; i < temp.Rows; i++)
                for (var j = 0; j < temp.Cols; j++)
                    temp[i, j] = matrix1[i, j] - matrix2[i, j];

            return temp;
        }
        
        public Matrix Substract(Matrix matrix)
        {
            if (matrix == null)
                throw new ArgumentNullException();
            
            if (this.Rows != matrix.Rows || this.Cols != matrix.Cols)
            {
                throw new ArgumentException("Wrong dimensions of matrix! They must be equal.");
            }

            return Matrix.Substract(this, matrix);
        }

        // Multiplication matrix by value.
        public static Matrix Multiply(Matrix matrix, double value)
        {
            if (matrix == null)
                throw new ArgumentNullException("matrix");

            Matrix temp = new Matrix(matrix.Rows, matrix.Cols);
            for (var i = 0; i < temp.Rows; i++)
                for (var j = 0; j < temp.Cols; j++)
                    temp[i, j] = matrix[i, j] * value;

            return temp;
        }

        public Matrix Multiply(double value)
        {
            return Matrix.Multiply(this, value);
        }
        
        // Multiplication of 2 matrix.
        public static Matrix Multiply(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null || matrix2 == null)
                throw new ArgumentNullException();

            if (matrix1.Rows != matrix2.Cols)
            {
                throw new ArgumentException("Wrong dimensions of matrix!");
            }

            Matrix temp = new Matrix(matrix1.Rows, matrix2.Cols);
            for (var i = 0; i < temp.Rows; i++)
                for (var j = 0; j < temp.Cols; j++)
                    for (var k = 0; k < matrix1.Cols; k++)
                        temp[i, j] = matrix1[i, k] * matrix2[k, j];

            return temp;
        }

        public Matrix Multiply(Matrix matrix)
        {
            if (matrix == null)
                throw new ArgumentNullException();

            if (this.Rows != matrix.Cols)
            {
                throw new ArgumentException("Wrong dimensions of matrix!");
            }
            
            return Matrix.Multiply(this, matrix);
        }
        
        // Get submatrix.
        public static Matrix GetSubMatrix(Matrix matrix, int startRow, int startCol, int endRow, int endCol)
        {
            int rowsRange = endRow - startRow;
            int colsRange = endCol - startCol;

            if (matrix == null)
                throw new ArgumentNullException("matrix");
         
            if (rowsRange < 1 || colsRange < 1 || rowsRange <= 0 || colsRange <= 0 
                || startRow < 0 || startCol < 0 || startRow >= matrix.Rows || startCol >= matrix.Cols
                || endRow < 0 || endCol < 0 || endRow >= matrix.Rows || endCol >= matrix.Cols)
                throw new ArgumentOutOfRangeException();

            var tempM = new Matrix(endRow - startRow + 1, endCol - startCol + 1);
            int newI = 0;
            
            for (var i = startRow; i <= endRow; i++)
            {
                int newJ = 0;
                for (var j = startCol; j <= endCol; j++)
                {
                    tempM[newI, newJ] = matrix[i, j];
                    newJ++;
                }
                newI++;
            }
            
            return tempM;
        }
        
        public Matrix GetSubMatrix(int startRow, int startCol, int endRow, int endCol)
        {
            int rowsRange = endRow - startRow;
            int colsRange = endCol - startCol;
         
            if (rowsRange < 1 || colsRange < 1 || rowsRange <= 0 || colsRange <= 0 
                || startRow < 0 || startCol < 0 || startRow >= this.Rows || startCol >= this.Cols
                || endRow < 0 || endCol < 0 || endRow >= this.Rows || endCol >= this.Cols)
                throw new ArgumentOutOfRangeException();
           
            return Matrix.GetSubMatrix(this, startRow, startCol, endRow, endCol);
        }

        // Transpose matrix.
        public static Matrix Transpose(Matrix matrix)
        {
            if (matrix == null)
                throw new ArgumentNullException("matrix");

            Matrix temp = new Matrix(matrix.Cols, matrix.Rows);
            for (int i = 0; i < matrix.Rows; i++)
                for (int j = 0; j < matrix.Cols; j++)
                    temp[j, i] = matrix[i, j];
            
            return temp;
        }
        
        public Matrix Transpose()
        {
            return Matrix.Transpose(this);
        }

        // Power of matrix.
        public static Matrix Power(Matrix matrix, double value)
        {
            if (matrix == null)
                throw new ArgumentNullException("matrix");

            Matrix temp = new Matrix(matrix.Rows, matrix.Cols);
            for (int i = 0; i < matrix.Rows; i++)
                for (int j = 0; j < matrix.Cols; j++)
                    temp[i, j] = Math.Pow(matrix[i, j], value);

            return temp;
        }
        
        public Matrix Power(double value)
        {
            return Matrix.Power(this, value);
        }

        // For output
        public override string ToString()
        {
            string res = "";

            for (var i = 0; i < Rows; i++)
            {
                for (var j = 0; j < Cols; j++)
                {
                    res += String.Format("{0}\t", _matrix[i, j]);
                }
                res += Environment.NewLine;
            }

            return res;
        }

        public override bool Equals(object other)
        {
            bool result = false;

            if (other is Matrix)
            {
                result = Equals((Matrix) other);
            }

            return result;
        }

        // Safe method
        public bool Equals(Matrix other)
        {
            return AreEqualsMatrixes(this, other);
        }

        public override int GetHashCode()
        {
            int hashCode = 0;
            foreach (var value in _matrix)
            {
                hashCode += (int)value;
            }

            return (int)(hashCode / 2) + _matrix.Length + base.GetHashCode();
        }
        #endregion

        #region Operators
        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            return Matrix.Add(matrix1, matrix2);
        }

        public static Matrix operator -(Matrix matrix)
        {
            return Matrix.Multiply(matrix, -1);
        }
        public static Matrix operator -(Matrix matrix1, Matrix matrix2)
        {
            return Matrix.Substract(matrix1, matrix2);
        }

        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            return Matrix.Multiply(matrix1, matrix2);
        }

        public static Matrix operator *(Matrix matrix, double multiplier)
        {
            return Matrix.Multiply(matrix, multiplier);
        }

        public static Matrix operator *(double multiplier, Matrix matrix)
        {
            return Matrix.Multiply(matrix, multiplier);
        }

        public static bool operator ==(Matrix matrix1, Matrix matrix2)
        {
            return Matrix.AreEqualsMatrixes(matrix1, matrix2);
        }

        public static bool operator !=(Matrix matrix1, Matrix matrix2)
        {
            return !Matrix.AreEqualsMatrixes(matrix1, matrix2);
        }
        #endregion
        
        #region Private methods
        // Check for equality of matrix.
        private static bool AreEqualsMatrixes(Matrix firstM, Matrix secondM)
        {
            bool areEquals = true;
            
            if ((object)firstM == null || (object)secondM == null || firstM.Rows != secondM.Rows || firstM.Cols != secondM.Cols)
            {
                areEquals = false;
            }
            else
            {
                for (var i = 0; i < firstM.Rows; i++)
                {
                    for (var j = 0; j < secondM.Cols; j++)
                    {
                        if (firstM[i, j] != secondM[i, j])
                        {
                            areEquals = false;
                            break;
                        }
                    }
                }
            }

            return areEquals;
        }
        #endregion
    }

    class Program
    {
        private static Random rnd = new Random();
        
        public static void RandomFill(Matrix matrix)
        {
            if (matrix == null)
                throw new ArgumentNullException("matrix");
            
            for (var i = 0; i < matrix.Rows; i++)
            {
                for (var j = 0; j < matrix.Cols; j++)
                {
                    matrix[i, j] = rnd.Next(100);
                }
            }
        }
        
        static void Main()
        {
            // Створення об'єктів.
            var m1 = new Matrix(5, 5);
            var m2 = new Matrix(5, 5);

            // Заповнення випадковими числами.
            RandomFill(m1);
            RandomFill(m2);

            // Вивід на консоль матриць.
            Console.WriteLine("Matrix m1:\n{0}", m1.ToString());
            Console.WriteLine("Matrix m2:\n{0}", m2.ToString());

            // Додавання 2-х матриць.
            Console.WriteLine("Summary of m1 and m2:");
            var sumMatrix = m1 + m2;
            Console.WriteLine(sumMatrix.ToString());

            // Віднімання 2-х матриць.
            Console.WriteLine("Substraction of m1 and m2:");
            var substactMatrix = m1 - m2;
            Console.WriteLine(substactMatrix.ToString());
            
            // Множення 2-х матриць.
            Console.WriteLine("Multiplication of m1 and m2:");
            var multipMatrix = m1 * m2;
            Console.WriteLine(multipMatrix.ToString());
            
            // Множення матриці на число.
            Console.WriteLine("Multiplication by value of m1 and m2:");
            var multipByValueMatrix = m1 * 2;
            // Можна й так
            // var multipByValueMatrix = 2 * m1;
            Console.WriteLine(multipByValueMatrix.ToString());

            // Транспонована матриця.
            Console.WriteLine("Transpose matrix:");
            var transposeM = m1.Transpose();
            Console.WriteLine(transposeM.ToString());
            
            // Отримання підматриці довільного розміру.
            Console.WriteLine("Submatrix of m1:");
            var subMatrix = Matrix.GetSubMatrix(m1, 1, 1, 3, 2);
            Console.WriteLine(subMatrix.ToString());

            // Піднесення до степеня.
            Console.WriteLine("Power of matrix:");
            var powerMatrix = Matrix.Power(m1, 2);
            Console.WriteLine(powerMatrix.ToString());

            // Порівняння 2-х матриць.
            Console.WriteLine("m1==m2? {0}", m1 == m2);
            
            // Заповнення 2-х матриць одинаковими даними
            // + доступ до масиву(індексатор).
            var equalMatrix1 = new Matrix(10, 7);
            var equalMatrix2 = new Matrix(10, 7);

            for (var i = 0; i < equalMatrix1.Rows; i++)
            {
                for (var j = 0; j < equalMatrix1.Cols; j++)
                {
                    equalMatrix1[i, j] = equalMatrix2[i, j] = i + j;
                }
            }

            Console.WriteLine("equalMatrix1=equalMatrix2? {0}", equalMatrix1 == equalMatrix2);
        }
    }
}